# docker build --rm -t hurence/spark  .
# docker tag hurence/spark:latest hurence/spark:2.4.3
FROM anapsix/alpine-java:8_jdk_nashorn

ARG spark_version=3.0.0
ARG scala_version=2.11

MAINTAINER hurence

RUN apk add --update unzip wget curl docker jq coreutils procps vim

VOLUME ["/spark"]

# Spark
RUN curl -s ftp://ftp.crihan.fr/mirrors/www.apache.org/dist/spark/spark-${spark_version}/spark-${spark_version}-bin-hadoop2.7.tgz | tar -xz -C /opt/
RUN cd /opt && ln -s spark-${spark_version}-bin-hadoop2.7 spark
ENV SPARK_HOME /opt/spark
ENV PATH $PATH:$SPARK_HOME/bin

RUN wget https://downloads.mysql.com/archives/get/p/3/file/mysql-connector-java-5.1.45.tar.gz
RUN tar xf mysql-connector-java-5.1.45.tar.gz
RUN mv mysql-connector-java-5.1.45/mysql-connector-java-5.1.45-bin.jar $SPARK_HOME/jars/
RUN rm -rf mysql-connector-java-5.1.45

WORKDIR $SPARK_HOME/

RUN mkdir /opt/jmx; cd /opt/jmx; wget https://repo1.maven.org/maven2/io/prometheus/jmx/jmx_prometheus_javaagent/0.9/jmx_prometheus_javaagent-0.9.jar
ADD jmx_prometheus.yml /opt/jmx/jmx_prometheus.yml

ADD metrics.properties /opt/spark/conf/metrics.properties
